window.addEventListener("DOMContentLoaded", async () => {
  const getConferenceUrl = "http://localhost:8000/api/conferences/";
  try {
    const conferenceResponse = await fetch(getConferenceUrl);
    if (!conferenceResponse.ok) {
      console.error("conferenceResponse is not OK!");
    } else {
      const data = await conferenceResponse.json();
      const conferenceTag = document.getElementById("conference");

      for (let conference of data.conferences) {
        const newOption = document.createElement("option");
        newOption.innerHTML = conference.name;
        newOption.setAttribute("value", conference.id);
        conferenceTag.appendChild(newOption);
      }

      const formTag = document.getElementById("create-presentation-form");
      formTag.addEventListener("submit", async (event) => {
        // Test Data
        // {
        //   "presenter_name": "Lina",
        //   "presenter_email": "lina@example.com",
        //   "company_name": "Example, Inc.",
        //   "title": "A Deep-Dive Into Django Views",
        //   "synopsis": "Come learn about the different..."
        // }
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const selectedConference = document.getElementById("conference")
        const selectedConferenceId = selectedConference.options[selectedConference.selectedIndex].value;

        const presentationUrl = `http://localhost:8000/api/conferences/${selectedConferenceId}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: json,
          header: {
            "Content-Type": "application/json",
          },
        };
        const presentationFormResponse = await fetch(presentationUrl, fetchConfig);
        if (!presentationFormResponse.ok) {
          console.error("presentationFormResponse is not OK!");
        } else {
          formTag.reset();
          const newPresentation = await presentationFormResponse.json();
          console.log(newPresentation);
        }
      });
    }
  } catch (e) {
    console.error(e);
  }
});
