window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const errorTestUrl = "http://localhost:8000/api/conferencesSOMETYPOERROR/";

  try {
    const response = await fetch(url);
    // const errorCatchTest = await fetchTYPOERROR(url);

    if (!response.ok) {
      const rowTag = document.querySelector('.row');
      const errorHtml = createErrMsg("Error, response.ok is false!");
      rowTag.innerHTML = errorHtml;
    } else {
      const data = await response.json();

      for (let index=0; index<data.conferences.length; index++) {
        const conference = data.conferences[index];

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();
          const location = details.conference.location.name;

          const html = createCard(title, description, pictureUrl, startDate, endDate, location);

          const col_index = index % 3;

          const columns = document.querySelectorAll(".col");
          console.log(columns)

          columns[col_index].innerHTML += html;
        }
      }
    }
  } catch (e) {
    const rowTag = document.querySelector('.row');
    const errorHtml = createErrMsg(e);
    rowTag.innerHTML = errorHtml;
  }

  function createCard(title, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card mb-4 shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          ${startDate} - ${endDate}
        </div>
      </div>
    `;
  }

  function createErrMsg(errorMessage){
    return `
      <div class="alert alert-danger d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
          ${errorMessage}
        </div>
      </div>
    `;
  }

});
