window.addEventListener("DOMContentLoaded", async () => {
  const getStatesUrl = "http://localhost:8000/api/states/";
  try {
    const stateResponse = await fetch(getStatesUrl);
    if (!stateResponse.ok) {
      console.error("stateResponse is not OK!");
    } else {
      const data = await stateResponse.json();
      const stateTag = document.getElementById("state");

      for (let state of data) {
        const newOption = document.createElement("option");
        newOption.innerHTML = state.name;
        newOption.setAttribute("value", state.abbreviation);
        stateTag.appendChild(newOption);
      }

      const formTag = document.getElementById("create-location-form");
      formTag.addEventListener("submit", async (event) => {
        // Test Data
        // {
        //   "name": "Moscone Center",
        //   "city": "San Francisco",
        //   "room_count": 62,
        //   "state": "CA"
        // }
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = "http://localhost:8000/api/locations/";
        const fetchConfig = {
          method: "post",
          body: json,
          header: {
            "Content-Type": "application/json",
          },
        };
        const locationFormResponse = await fetch(locationUrl, fetchConfig);
        if (!locationFormResponse.ok) {
          console.error("locationFormResponse is not OK!");
        } else {
          formTag.reset();
          const newLocation = await locationFormResponse.json();
          console.log(newLocation);
        }
      });
    }
  } catch (e) {
    console.error(e);
  }
});
