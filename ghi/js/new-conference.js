window.addEventListener("DOMContentLoaded", async () => {
  const getLocationUrl = "http://localhost:8000/api/locations/";
  try {
    const locationResponse = await fetch(getLocationUrl);
    if (!locationResponse.ok) {
      console.error("locationResponse is not OK!");
    } else {
      const data = await locationResponse.json();
      const locationTag = document.getElementById("location");

      for (let location of data.locations) {
        const newOption = document.createElement("option");
        newOption.innerHTML = location.name;
        newOption.setAttribute("value", location.id);
        locationTag.appendChild(newOption);
      }

      const formTag = document.getElementById("create-conference-form");
      formTag.addEventListener("submit", async (event) => {
        // Test Data
        // {
        //   "name": "WWDC",
        //   "starts": "02/20/2025",
        //   "ends": "02/27/2025",
        //   "description": "The premier event to learn...",
        //   "max_presentations": 120,
        //   "max_attendees": 20000,
        //   "location": 3
        // }
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
          method: "post",
          body: json,
          header: {
            "Content-Type": "application/json",
          },
        };
        const conferenceFormResponse = await fetch(conferenceUrl, fetchConfig);
        if (!conferenceFormResponse.ok) {
          console.error("conferenceFormResponse is not OK!");
        } else {
          formTag.reset();
          const newConference = await conferenceFormResponse.json();
          console.log(newConference);
        }
      });
    }
  } catch (e) {
    console.error(e);
  }
});
