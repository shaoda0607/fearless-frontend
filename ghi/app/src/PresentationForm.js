import React, { useEffect, useState } from "react";

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [name, setName] = useState("");
  const [presenterEmail, setPresenterEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePresenterEmailChange = (event) => {
    const value = event.target.value;
    setPresenterEmail(value);
  };

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  };

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  };

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handleSubmit = async (event) => {
    // Test Data
    // {
    //   "presenter_name": "Lina",
    //   "presenter_email": "lina@example.com",
    //   "company_name": "Example, Inc.",
    //   "title": "A Deep-Dive Into Django Views",
    //   "synopsis": "Come learn about the different..."
    // }
    event.preventDefault();
    const data = {};
    data.presenter_name = name;
    data.presenter_email = presenterEmail;
    data.company_name = companyName;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;
    console.log(data);

    const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      header: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      setName("");
      setPresenterEmail("");
      setCompanyName("");
      setTitle("");
      setSynopsis("");
      setConference("");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      console.log(data.conferences);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                value={name}
                placeholder="Presenter name"
                required
                type="text"
                id="presenter-name"
                className="form-control"
                name="presenter_name"
              />
              <label htmlFor="presenter-name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePresenterEmailChange}
                value={presenterEmail}
                required
                placeholder="Presenter email"
                type="email"
                id="presneter-email"
                className="form-control"
                name="presenter_email"
              />
              <label htmlFor="presneter-email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleCompanyNameChange}
                value={companyName}
                placeholder="Company name"
                type="text"
                id="company-name"
                className="form-control"
                name="company_name"
              />
              <label htmlFor="company-name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleTitleChange}
                value={title}
                required
                placeholder="Title"
                type="text"
                id="title"
                className="form-control"
                name="title"
              />
              <label htmlFor="title">Title</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={handleSynopsisChange}
                value={synopsis}
                required
                placeholder="Synopsis"
                style={{ height: "200px" }}
                id="synopsis"
                className="form-control"
                name="synopsis"
              ></textarea>
              <label htmlFor="synopsis">Synopsis</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleConferenceChange}
                value={conference}
                required
                id="conference"
                className="form-select"
                name="conference"
              >
                <option value="">Choose a conference</option>
                {conferences.map((conference) => {
                  return (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
